# StreamTG

Stream files from Telegram  

### Installation Instructions
1. Install `python3`
2. `pip3 install -r requirements.txt`
3. Copy example-config.yaml to config.yaml and edit it

### Start
`python3 streamtg.py`  
If you want to set a custom port, have a PORT environment variable

### How to Use
http://localhost:8080/?chat_id=-1001289824958&message_id=5302&message_id=5304&token=DIi4aXHn440PTPXJE1yVyIoU2L4bLGiyjC1Fd7usKAMZYWVcp5p0P792G4YlbNnIcWCLypXbUFJkVzKqhh0AkJYSWqJbsAy8TjA
